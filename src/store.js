import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    items: JSON.parse(window.localStorage.getItem('todoList') || '[]')
  },
  mutations: {
    addItem (state, item) {
      state.items.push(item)
    },
    initItem (state, items) {
      state.items = items
    }
  }
})
